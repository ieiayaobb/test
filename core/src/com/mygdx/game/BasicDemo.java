package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class BasicDemo extends ApplicationAdapter {
	private SpriteBatch batch;
	private Sprite sprite;
	private OrthographicCamera camera;
	private TextureRegion region;
	private Texture texture;
	
	
    private ShapeRenderer shapeRenderer;
	
	@Override
	public void create(){
		
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
        shapeRenderer = new ShapeRenderer();
        
        float cameraWidth = Gdx.graphics.getWidth();
        float cameraHeight = Gdx.graphics.getHeight();
        camera.setToOrtho(false, cameraWidth, cameraHeight);
        camera.position.set(0, 0, 0);
        
        camera.update();
        
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.line(1, 1, 100, 100);
        shapeRenderer.end();
		
		
		Gdx.input.setInputProcessor(new InputAdapter () {
			   public boolean touchDown (int x, int y, int pointer, int button) {
				  Gdx.app.log("BasicDemo", "touchDown");
			      return true; // return true to indicate the event was handled
			   }

			   public boolean touchUp (int x, int y, int pointer, int button) {
				   Gdx.app.log("BasicDemo", "touchUp");
			      return true; // return true to indicate the event was handled
			   }
			   public boolean touchDragged (int x, int y, int pointer) {
				   Gdx.app.log("BasicDemo", "touchDragged");
				   Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
				   texture = new Texture(Gdx.files.internal("badlogic.jpg"));
				   sprite = new Sprite(texture,0,0,50,50);
				   batch.begin();
				   sprite.setPosition(x, 600 - y);
				   sprite.draw(batch);
				   batch.end();
				   
			      return true;
			   }
			});
	}
	@Override
	public void render(){
		
		
		
//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//		texture = new Texture(Gdx.files.internal("badlogic.jpg"));
//		region = new TextureRegion(texture, 20, 20, 50, 50);
//		sprite = new Sprite(texture,20,20,50,50);
//		sprite.setPosition(100, 10);
//		sprite.setRotation(45);
//		sprite.setColor(0,0,1,1);
//		
//		batch.begin();
//		batch.setColor(1,0,0,1);
//		batch.draw(texture, 10, 10);
//		
//		batch.setColor(0,1,0,1);
//		batch.draw(region, 10, 10);
//		
//		sprite.draw(batch);
//		batch.end();
	}
}
