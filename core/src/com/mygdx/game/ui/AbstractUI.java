package com.mygdx.game.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;

public abstract class AbstractUI {
	protected ShapeRenderer shapeRenderer;
	
	public abstract void paint();
	
	public void rotate(float degrees){
		Gdx.app.log("rotate", degrees + "");
		this.shapeRenderer.rotate(0, 0, 1, degrees);
	}
	
}
