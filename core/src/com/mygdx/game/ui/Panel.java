package com.mygdx.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;

public class Panel extends AbstractUI{
	
	public Panel(Matrix4 matrix){
		shapeRenderer = new ShapeRenderer();
		this.shapeRenderer.setProjectionMatrix(matrix);
	}

	@Override
	public void paint() {
		shapeRenderer.begin(ShapeType.Point);
		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.circle(0, 0, 100);
		shapeRenderer.end();
	}
}
