package com.mygdx.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class DragPathWay extends AbstractUI{

	public DragPathWay() {
		this.shapeRenderer = new ShapeRenderer();
	}
	@Override
	public void paint() {
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.RED);
		shapeRenderer.line(0, 0, 100, 100);
		shapeRenderer.end();
		
	}

}
