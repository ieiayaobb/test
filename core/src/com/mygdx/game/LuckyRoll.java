package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.ui.DragButton;
import com.mygdx.game.ui.Pointer;

public class LuckyRoll extends ApplicationAdapter {
	private SpriteBatch batch;
	private Sprite arrow;
	
	private Sprite drag;
	
	private Texture texture;
	
	@Override
	public void create(){
		batch = new SpriteBatch();
		
		texture = new Texture(Gdx.files.internal("badlogic.jpg"));
		arrow = new Sprite(texture,100,100,20,200);
		arrow.setPosition(300, 700);
		
		drag = new Sprite(texture,0,0,100,100);
		drag.setPosition(100, 100);
		
		Gdx.input.setInputProcessor(new InputAdapter () {
		   public boolean touchDown (int x, int y, int pointer, int button) {
			  Gdx.app.log("BasicDemo", "touchDown");
		      return true; // return true to indicate the event was handled
		   }

		   public boolean touchUp (int x, int y, int pointer, int button) {
			   Gdx.app.log("BasicDemo", "touchUp");
			   
			   speed = (x - 100) / 20;
			   
			   drag.setPosition(100, 100);
			   
		      return true; // return true to indicate the event was handled
		   }
		   public boolean touchDragged (int x, int y, int pointer) {
			   Gdx.app.log("BasicDemo", "touchDragged");
			   Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			   
			   if(x > 400){
				   x = 400;
			   }
			   
			   batch.begin();
			   drag.setPosition(x, 100);
			   drag.draw(batch);
			   arrow.draw(batch);
			   batch.end();
			   
		      return true;
		   }
		});
		
	}
	private float angle = 0f;
	private float speed = 0f;
	private float friction = - 0.1f;
	
	@Override
	public void render(){
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		arrow.setOrigin(0, 10);
		angle += speed;
		arrow.setRotation(angle);
		
		batch.begin();
		arrow.draw(batch);
		drag.draw(batch);
		batch.end();
		if(speed > 0){
			speed += friction;
		}
	}
}
