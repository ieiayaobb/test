package com.mygdx.game.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;

public class Pointer extends AbstractUI{
	
	public Pointer(Matrix4 matrix) {
		this.shapeRenderer = new ShapeRenderer();
		this.shapeRenderer.setProjectionMatrix(matrix);
	}
	@Override
	public void paint() {
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.RED);
		shapeRenderer.line(0, 0, 100, 100);
		shapeRenderer.end();
		
	}
}
